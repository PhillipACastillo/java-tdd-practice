import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

public class Calculator {
    int add (String number) throws NegativeException {

        if (number == "") {
            return 0;

            // Take first two // (forward-slashes)
        } else if (number.charAt(0) == '/' && number.charAt(1) == '/') {


            // Take third character from string
            char delimiter = number.charAt(2);

            // find newline character and take substring after "n"
            int index = number.indexOf("\n");
            String substring = number.substring(index + 1);
            if(delimiter == '[') {
                String[] delimiterArr = getDelimiters((number));
                String pipedDelimiters = "";
                for (String separator : delimiterArr) {
                    pipedDelimiters += separator + "|";
                }
                String[] numArr =
//                        substring.split(Pattern.quote(getDelimiters(number)));
                        substring.split(pipedDelimiters.substring(0,
                                pipedDelimiters.length() - 1));
                return sum(numArr);
            }

            // take the new string and split on previously saved third character
            String[] numArr = substring.split(Character.toString(delimiter));
            return sum(numArr);
        } else if (number.contains(",")) {
            String[] numArr = number.split("\\n|,");
            return sum(numArr);
        }  else {
            return parseInt(number);
        }
    }

    String[] getDelimiters(String number) {
        //Find the index of the closing square bracket
        int newlineIndex = number.indexOf("\n");

        //return "\\" + number.substring(3, closingBracketIndex) + "\\";
        String str =  number.substring(3, newlineIndex - 1);
        String[] strArr = str.split(Pattern.quote("]["));
        return escapeDelimiters(strArr).toArray(new String[0]);
    }

    ArrayList<String> escapeDelimiters(String[] unescapedDelimiters) {
        // [\\*, \\%]
        ArrayList<String> escaped = new ArrayList<>();
        for (String unescaped : unescapedDelimiters) {
            if (unescaped.contains("*") || unescaped.contains("%")
                || unescaped.contains("^")) {
                String multicharacter = "";
                for (int i = 0; i < unescaped.length(); i++ ) {
                    if (unescaped.charAt(i) == '*' || unescaped.charAt(i) == '%'
                    || unescaped.charAt(i) == '^') {
                        multicharacter += "\\" + Character.toString(unescaped.charAt(i));
                    }
                }
                escaped.add(multicharacter);
            } else {
                escaped.add(unescaped);
            }
        }
        return escaped;
    }


    int sum (String[] numArr) throws NegativeException{
        int total = 0;
        Set<String> negatives = new HashSet<>();

        for(String num : numArr) {
            if (parseInt(num) < 0) {
                // add to array (of negatives)
                negatives.add(num);
            } else if (parseInt(num) > 1000){
                continue;
            }
            total+= parseInt(num);
        }

        // if the array of negatives has a size greater than 0 then
        // throw
        if(negatives.size() > 0) {
            StringBuilder negativeNums = new StringBuilder();
            for(String num : negatives) {
                negativeNums.append(num + ",");
            }
            throw new NegativeException("negatives not allowed: "
                    + negativeNums.substring(0, negativeNums.length() - 1));
        }
        return total;
    }
}
