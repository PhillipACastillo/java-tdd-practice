import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestCalculator {

    Calculator calculator;
    @BeforeEach
    public void instantiateCalculator() {
        calculator = new Calculator();
    }

    @Test
    public void takesEmptyString() throws NegativeException {
        //SETUP
        int expected = 0;

        //EXECUTION
        int actual = calculator.add("");

        //ASSERT
        assertEquals(expected, actual, "Should return 0");
    }

    @Test
    public void takesStringWithOneNumber() throws NegativeException{
        //SETUP
        int expected = 1;

        //EXECUTION
        int actual = calculator.add("1");

        //ASSERT
        assertEquals(expected, actual, "Should return 1");
    }

    @Test
    public void takesStringWithTwoNumbers() throws NegativeException{
        //SETUP
        int expected = 3;

        //EXECUTION
        int actual = calculator.add("1,2");

        //ASSERT
        assertEquals(expected, actual, "Should return 3");
    }

    @Test
    public void takesStringWithMultipleNumbers() throws NegativeException{
        //SETUP
        int expected = 6;

        //EXECUTION
        int actual = calculator.add("1,2,3");

        //ASSERT
        assertEquals(expected, actual, "Should return 6");
    }

    @Test
    public void takesStringWithNewLineCharacters() throws NegativeException{
        //SETUP
        int expected = 6;

        //EXECUTION
        int actual = calculator.add("1\n2,3");

        //ASSERT
        assertEquals(expected, actual, "Should return 6");
    }

    @Test
    public void takesStringWithNewDelimiter() throws NegativeException{
        //SETUP
        int expected = 3;

        //EXECUTION
        int actual = calculator.add("//;\n1;2");

        //ASSERT
        assertEquals(expected, actual, "Should return 3");
    }

    @Test
    public void throwsExceptionWithNegative() {
        //SETUP

        //EXECUTION
        NegativeException exception = assertThrows(NegativeException.class, () -> {
            calculator.add("1,-1");
        });

        //ASSERT
        assertEquals("negatives not allowed: -1", exception.getMessage());
    }

    @Test
    public void throwsExceptionWithMultipleNegatives() {
        //SETUP

        //EXECUTION
        NegativeException exception = assertThrows(NegativeException.class, () -> {
            calculator.add("1,-1,-2,-3");
        });

        //ASSERT
        assertEquals("negatives not allowed: -1,-2,-3",
                exception.getMessage());
    }

    @Test
    public void takeStringWithNumberBiggerThanThousand() throws NegativeException {
        //SETUP
        int expected = 2;

        //EXECUTION
        int actual = calculator.add("2,1001");

        //ASSERT
        assertEquals(expected, actual, "Should return 2");
    }

    @Test
    public void takeStringWithMultipleCharacterDelimiters() throws NegativeException {
        //SETUP
        int expected = 6;

        //EXECUTION
        int actual = calculator.add("//[***]\n1***2***3");

        //ASSERTION
        assertEquals(expected,actual, "Should return 6");
    }

    @Test
    public void takeStringWithMultipleDelimiters() throws NegativeException {
        //SETUP
        int expected = 6;

        //EXECUTION
        int actual = calculator.add("//[*][%]\n1*2%3");

        //ASSERTION
        assertEquals(expected,actual, "Should return 6");
    }

}
