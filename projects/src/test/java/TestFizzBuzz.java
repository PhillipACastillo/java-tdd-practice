import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// Return to these tests and attempt to make parameterized
public class TestFizzBuzz {

    FizzBuzz fizzbuzz;

    @BeforeEach
    public void instantiateFizzBuzz() {
        fizzbuzz = new FizzBuzz();
    }


    @Test
    public void printsFizz() {
        //SETUP
        String expected = "Fizz";

        //EXECUTION
        String actual = fizzbuzz.print(3);

        //ASSERT
        assertEquals(expected, actual, "It should print Fizz");
    }

    @Test
    public void printsBuzz() {
        //SETUP
        String expected = "Buzz";

        //EXECUTION
        String actual = fizzbuzz.print(5);

        //ASSERT
        assertEquals(expected, actual, "It should print Buzz");
    }

    @Test
    public void printsFizzBuzz() {
        //SETUP
        String expected = "FizzBuzz";

        //EXECUTION
        String actual = fizzbuzz.print(15);

        //ASSERT
        assertEquals(expected, actual, "It should print FizzBuzz");
    }

    @Test
    public void printsNumberAsString() {
        //SETUP
        String expected = "4";

        //EXECUTION
        String actual = fizzbuzz.print(4);

        //ASSERT
        assertEquals(expected, actual, "It should print 4");
    }
}
